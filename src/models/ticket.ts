export{};
const mongoose = require('mongoose');
const validator = require("validator");

const ticketSchema = new mongoose.Schema({
    name : {
        type:String,
        required : true
    },
    age : {
        type : Number,
        required : true,
    },
    busNumber : {
        type : Number,
        required : true
    },
    seatNumber : {
        type : Number,
        required : true
    },
    status : {
        type : String
    },
    owner: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    }
})

const Ticket = mongoose.model('Ticket',ticketSchema);

module.exports = Ticket