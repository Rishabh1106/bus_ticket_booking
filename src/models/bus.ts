const mongoose = require('mongoose');
const validator = require("validator");


const busSchema = new mongoose.Schema({
    number : {
        type : Number,
        required : true,
        unique : true
    },
    source : {
        type : String,
        required : true
    },
    destination : {
        type : String,
        required : true
    },
    totalSeats : {
        type : Number,
        required : true
    },
    seats : {
        type : [Number]
    }
})


const Bus = mongoose.model('Bus',busSchema);

module.exports = Bus;