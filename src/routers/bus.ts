
import * as express from 'express';

const router = express.Router();

const Bus = require("../models/bus");
const Ticket = require("../models/ticket");
const auth = require("../middleware/auth");



// Admin end points


router.post("/addBus", auth, async (req:any, res) => {
    if(req.user.role != "admin"){
      res.status(401).send("You're not an admin!")
    }else{
    const bus = new Bus(req.body);
    for(let i=0;i<bus.totalSeats;i++){
      bus.seats[i] = i+1;
    }
    try {
      await bus.save();
      res.status(201).send(bus);
    } catch (e) {
        console.log(e);
      res.status(400).send(e);
    }
    }
  });


router.post('/resetBus',auth, async (req:any ,res)=>{
  if(req.user.role != "admin"){
    res.status(401).send("You're not an admin!")
  }
  const bus = await Bus.findOne({number : req.body.number});
  for(let i=0;i<bus.totalSeats;i++){
    bus.seats[i] = i+1;
  }
  await bus.save();
  res.send(bus);
})


router.get('/getAllTickets', auth, async (req:any,res)=>{
  if(req.user.role != "admin"){
    res.status(401).send("You're not an admin!")
  }else{
    const tickets = await Ticket.find({});
    res.send(tickets);
  }
  })

// User and common Routes

router.get('/getAllBuses', auth, async (req,res)=>{
  const buses = await Bus.find({});
  res.send(buses);
})

router.get('/getBus/:number', auth, async (req,res)=>{
  const number = req.params.number;
  const bus = await Bus.findOne({number:number})
  res.send(bus)
})

router.post('/bookTicket', auth, async (req:any,res)=>{
  const buses = await Bus.find({});
  let bus = buses.find(b=> b.number==req.body.busNumber);
  if(!bus){
    res.status(404).send("Bus not found please enter available bus number!");
  }
  let seatnumber = bus.seats.find((e)=>{return e==req.body.seatNumber});
  if(!seatnumber){
    res.status(404).send("This seat is not available please choose another number!")
  }
  if(bus && seatnumber){
  
  const ticket = new Ticket(req.body);
  ticket.status = "Confirmed"
  ticket.owner = req.user._id

  bus.seats = bus.seats.filter((s)=> {return s!=seatnumber})
  await bus.save();
  try {
      await ticket.save();
      res.status(201).send(ticket);
    } catch (e) {
        console.log(e);
        res.status(400).send(e);
    }
  }
})

router.post('/cancelTicket', auth, async (req, res) => {
      const _id = req.body.id;
      let ticket = await Ticket.findOne({_id : _id})
  
      
      ticket.status = "Cancelled";

      await ticket.save();
      res.send(ticket)
      
})



router.get('/getMyTickets', auth, async (req:any,res)=>{
    
    try {
      const tickets = await Ticket.find({owner : req.user._id})
      res.send(tickets)
    } catch (e) {
        console.log(e);
        res.status(400).send(e);
    }
    })
  



module.exports = router;