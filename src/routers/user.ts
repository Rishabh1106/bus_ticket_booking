export {};

import * as express from 'express';

const router = express.Router();

const User = require("../models/user");
const auth = require("../middleware/auth");


// admin endpoints

router.post("/registerAdmin", async (req, res) => {
  const user = new User(req.body);
  user.role = "admin";
  try {
    await user.save();
    const token = await user.generateAuthToken();
    res.status(201).send({ user, token });
  } catch (e) {
    res.status(400).send(e);
  }
});

router.post("/loginAdmin", async (req, res) => {
  try {
    const user = await User.findUserForlogin(req.body.email, req.body.password);
    const token = await user.generateAuthToken();
    if (user.role == "admin") res.send({ user, token });
    else res.send("You are not an admin!");
  } catch (e) {
    res.status(400).send(e);
  }
});

// user end points

router.post("/registerUser", async (req, res) => {
  const user = new User(req.body);
  user.role = "user";
  try {
    await user.save();
    const token = await user.generateAuthToken();
    res.status(201).send({ user, token });
  } catch (e) {
    res.status(400).send(e);
  }
});


router.post("/loginUser", async (req, res) => {
  try {
    const user = await User.findUserForlogin(req.body.email, req.body.password);
    const token = await user.generateAuthToken();
    if (user.role == "user") res.send({ user, token });
    else res.send("You are not a user!");
  } catch (e) {
    res.status(400).send(e);
  }
});

//common end points

router.post("/logout", auth, async (req:any, res:any) => {
  try {
    req.user.tokens = req.user.tokens.filter((token) => {
      return token.token !== req.token;
    });
    await req.user.save();

    res.send();
  } catch (e) {
    res.status(500).send();
  }
});


router.post("/logoutAll", auth, async (req:any, res:any) => {
  try {
    req.user.tokens = [];
    await req.user.save();
    res.send();
  } catch (e) {
    res.status(500).send();
  }
});

router.get("/users/me", auth, async (req:any, res:any) => {
  res.send(req.user);
});

module.exports = router;
