import path from 'path';
import express from 'express';
import * as bodyParser from 'body-parser';

require("./db/mongoose");
const userRouter = require("./routers/user");
const busRouter = require("./routers/bus");
const app = express();
app.use(
    bodyParser.urlencoded({
      extended: true,
    })
  );

  app.use(bodyParser.json());
  
  app.use(express.json());
  app.use(userRouter);
  app.use(busRouter);
  



app.listen(3000, () => {
    console.log("Server is Up and running at port 3000....");
  });